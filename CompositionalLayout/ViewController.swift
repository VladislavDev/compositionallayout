//
//  ViewController.swift
//  CompositionalLayout
//
//  Created by Vlad on 9/14/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    enum SectionKind: Int, CaseIterable {
        case list, grid3
        var columnCount: Int {
            switch self {
            case .list: return 2
            case .grid3: return 3
            }
        }
    }
    
    var dataSource: UICollectionViewDiffableDataSource<SectionKind, Int>! = nil
    var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
        setupDataSource()
        reloadData()
    }
    
    // MARK: - Setup data
    private func setupDataSource() {
        dataSource = UICollectionViewDiffableDataSource<SectionKind, Int>(
            collectionView: collectionView,
            cellProvider: { (collectionView, indexPath, intValue) -> UICollectionViewCell? in
                let section = SectionKind(rawValue: indexPath.section)!
                
                switch section {
                case .list: return self.configure(cellType: UserCell.self, with: intValue, for: indexPath)
                case .grid3: return self.configure(cellType: FoodCell.self, with: intValue, for: indexPath)
                }
            }
        )
    }
    
    private func configure<T: SelfConfiguringCell>(
        cellType: T.Type,
        with intValue: Int,
        for indexPath: IndexPath
    ) -> T {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: cellType.reuseId,
            for: indexPath) as? T else {
            fatalError("Fatar error cast cell")
        }
        
        return cell
    }
    
    private func reloadData() {
        var snapshot = NSDiffableDataSourceSnapshot<SectionKind, Int>()
        let itemPerSection = 10
        
        SectionKind.allCases.forEach { sectionKind in
            let itemOffSet = sectionKind.columnCount * itemPerSection
            let itemUpperbound = itemOffSet + itemPerSection
            
            snapshot.appendSections([sectionKind])
            snapshot.appendItems(Array(itemOffSet..<itemUpperbound))
        }
        
        dataSource.apply(snapshot, animatingDifferences: true)
    }
    
    private func setupCollectionView() {
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: createLayout())
        collectionView.backgroundColor = .clear
        
        collectionView.register(UserCell.self, forCellWithReuseIdentifier: UserCell.reuseId)
        collectionView.register(FoodCell.self, forCellWithReuseIdentifier: FoodCell.reuseId)
        
        collectionView.delegate = self
        
        self.view.addSubview(collectionView)
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: self.view.layoutMarginsGuide.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: self.view.layoutMarginsGuide.trailingAnchor),
        ])
    }

    // MARK: - Setup layout
    private func createLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { (sectionIdx, _) -> NSCollectionLayoutSection? in
            let section = SectionKind(rawValue: sectionIdx)!
            
            switch section {
            case .list: return self.createGridSection()
            case .grid3: return self.createListSection()
            }
        }
        
        return layout
    }
    
    private func createListSection() -> NSCollectionLayoutSection {
        let size = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .estimated(165)
        )
        let item = NSCollectionLayoutItem(layoutSize: size)
        
        let groupSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .estimated(165)
        )
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 2)
        // Вертикальный отступ между ячейками
        group.interItemSpacing = .fixed(16)
        
        let section = NSCollectionLayoutSection(group: group)
        // Вертикальный отступ между группами
        section.interGroupSpacing = 16
        // Отступы между секцаями
        section.contentInsets = NSDirectionalEdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0)
        
        return section
    }
    
    private func createGridSection() -> NSCollectionLayoutSection {
        let size = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1.0),
            heightDimension: .estimated(165)
        )
        let item = NSCollectionLayoutItem(layoutSize: size)
        
        let groupSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(0.8),
            heightDimension: .estimated(165)
        )
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        // Вертикальный отступ между ячейками
        group.interItemSpacing = .fixed(16)
        
        let section = NSCollectionLayoutSection(group: group)
        // Устанавилваем горизонтальный скролл
        section.orthogonalScrollingBehavior = .groupPaging
        // Вертикальный отступ между группами
        section.interGroupSpacing = 16
        // Отступы между секцаями
        section.contentInsets = NSDirectionalEdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0)
        
        return section
    }
}

extension ViewController: UICollectionViewDelegate {
    // Тап по ячейке
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("INDEX PATH: ", indexPath)
        
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}

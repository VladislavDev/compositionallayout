//
//  UserCell.swift
//  CompositionalLayout
//
//  Created by Vlad on 9/14/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

protocol SelfConfiguringCell {
    static var reuseId: String { get set }
    func configure(with intValue: Int)
}

final class UserCell: UICollectionViewCell, SelfConfiguringCell {
    static var reuseId: String = "UserCell"
    
    var friendImageView: UIImageView = {
        let v = UIImageView()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .blue
        
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .purple
        
        setupConstraints()
        
        self.layer.cornerRadius = 4
        self.clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupConstraints() {
        self.addSubview(friendImageView)
        
        friendImageView.frame = self.bounds
    }
    
    func configure(with intValue: Int) {
        print("123")
    }
}

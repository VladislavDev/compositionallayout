//
//  FoodCell.swift
//  CompositionalLayout
//
//  Created by Vlad on 9/14/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import UIKit

final class FoodCell: UICollectionViewCell, SelfConfiguringCell {
    static var reuseId: String = "FoodCell"
    
    var labelView: UILabel = {
        let v = UILabel()
        
        v.translatesAutoresizingMaskIntoConstraints = false
        
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .green
        
        setupConstraints()
        
        self.layer.cornerRadius = 4
        self.clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupConstraints() {
        self.addSubview(labelView)
        
        labelView.frame = self.bounds
        labelView.text = "123"
    }
    
    func configure(with intValue: Int) {
        print("123")
    }
}
